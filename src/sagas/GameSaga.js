import { put, takeLatest, select, take } from 'redux-saga/effects';
import { delay } from 'redux-saga';

const getGame = state => state.game;
const getTargets = state => state.targets;
var nbTargets = 0;
var colorPicker = {
    42: '#29a02b',
    9:'#3a9529',
    8:'#4a8b28',
    7:'#5b8126',
    6:'#7b6b23',
    5:'#8c6121',
    4:'#ad4c1e',
    3:'#ce371a',
    2:'#de2c19',
    1:'#ef2217'
}

function* gameStart() {
    yield put({type: "GAME_START"});

    let game = yield select(getGame);

    // Game loop
    while (game.isStarted) {
        yield delay(1000);

        if (game.score < 5) {
            yield addTarget(1);
        }
        else if (game.score > 15) {
            yield addTarget(3);
        }
        else {
            yield addTarget(2);
        }

        let targets = yield select(getTargets);
        // Decrease targets values
        targets = targets.map(element => {
            element.value--;

            // Set their background according to their value
            if (element.value >= 10)
                element.background = colorPicker[42];
            else
                element.background = colorPicker[element.value];

            return element;
        })
        // See if any targed has died by itself
        var lenPostRemoval = targets.length;
        targets = targets.filter(element => element.value !== 0);
        if (lenPostRemoval !== targets.length) {
            for (var i = 0; i < lenPostRemoval - targets.length; ++i) {
                yield put({type: 'TARGET_DIES'});
            }
        }

        // Update the state
        yield put({type: 'REMOVE_TARGET', newState: targets})

        game = yield select(getGame);
    }
}

function* gameStop() {
    yield put({type: 'GAME_STOP'});
}

function* removeTarget(action) {
    var targets = yield select(getTargets);
    var id = action.id;
    targets = targets.filter(element => element.id !== id);
    yield put({type: 'REMOVE_TARGET', newState: targets});
    yield put({type: 'TARGET_ELIMINATED'});
}

function* addTarget(nbNewTargets) {
    for (var i = 0; i < nbNewTargets; ++i) {
        yield put({type: 'ADD_TARGET', newElement: {
            value: Math.floor(Math.random() * 10 + 5),
            x: Math.floor(Math.random() * 90) + 5,
            y: Math.floor(Math.random() * 90) + 5,
            background: 'blue',
            id: nbTargets++
        }})
    }
}

function* gameSaga() {
    yield takeLatest("GAME_START_REQUESTED", gameStart);
    yield takeLatest("GAME_STOP_REQUESTED", gameStop);
    yield takeLatest("REMOVE_TARGET_REQUESTED", removeTarget);
}

export default gameSaga;