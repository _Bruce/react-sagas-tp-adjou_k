import React from 'react';
import { connect } from 'react-redux';
import Target from '../components/Target';
import Info from '../components/Info';
import ButtonStart from '../components/ButtonStart';
import ButtonStop from '../components/ButtonStop';

// FIXME: maybe, do something about this ?
const mapStateToProps = state => ({
  lives: state.game.lives,
  score: state.game.score,
  isStarted: state.game.isStarted,
  targets: state.targets
});

const GameLayout = ({ targets, isStarted, lives, score, dispatch }) => (
  <div
    style={{
      position: 'fixed',
      backgroundColor: '#21222C',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      width: '100vw',
      height: '100vh',
      margin: 'auto'
    }}
  >
    {isStarted ? (
      <React.Fragment>
        <ButtonStop onClick={() => dispatch({ type: 'GAME_STOP_REQUESTED' })}/>
        <Info lives={lives} score={score} />
        {targets.map(element => (
          <Target
            x={element.x} y={element.y}
            value={element.value}
            id={element}
            bg={element.background}
            onClick={() => dispatch({ type: 'REMOVE_TARGET_REQUESTED', id: element.id })}
            key={element.id}
             />
          ))}
      </React.Fragment>
    ) : (
      <ButtonStart onClick={() => dispatch({ type: 'GAME_START_REQUESTED' })} />
    )}
  </div>
);

export default connect(mapStateToProps)(GameLayout);
