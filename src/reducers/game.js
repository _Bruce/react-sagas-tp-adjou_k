const defaultState = {
  lives: 3,
  score: 0,
  isStarted: false
};

const game = (state = defaultState, action) => {
  switch (action.type) {
    case 'GAME_START':
      return {
        ...state,
        isStarted: true
      };
    case 'TARGET_DIES':
      return {
        ...state,
        lives: state.lives - 1
      };
    case 'TARGET_ELIMINATED':
      return {
        ...state,
        score: state.score + 1
      }
    case 'GAME_STOP':
      return defaultState;
    default:
      return state;
  }
};

export default game;
