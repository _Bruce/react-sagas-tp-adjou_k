const defaultState = {
  x: -1,
  y: -1,
  value: 3,
  id: -1
}

const target = (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state;
  }
}

const targets = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TARGET':
      return [...state, action.newElement];
    case 'REMOVE_TARGET':
      return action.newState;
    case 'GAME_START':
      return [];
    default:
      return state;
  }
}

export default targets;