import React from 'react';

const Target = ({ x = 0, y = 0, value = 0, id = 0, bg = '#FFD0650', onClick = () => {} }) => (
  <div
    style={{
      position: 'absolute',
      top: `${y}%`,
      left: `${x}%`,
      width: '25px',
      height: '25px',
      textAlign: 'center',
      lineHeight: '25px',
      cursor: 'pointer',
      backgroundColor: `${bg}`
    }}
    onClick={onClick}
    id={id}
  >
    {value}
  </div>
);

export default Target;
