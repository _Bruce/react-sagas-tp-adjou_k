import React from 'react';

const ButtonStop = ({ onClick = () => {} }) => (
  <div
    style={{
      position: 'fixed',
      backgroundColor: 'red',
      top: '10px',
      left: '10px',
      padding: '20px',
      cursor: 'pointer',
      borderRadius: '20px'
    }}
    onClick={onClick}>
      STOP
  </div>
);

export default ButtonStop;